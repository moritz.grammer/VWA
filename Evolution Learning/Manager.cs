using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[SelectionBase]
public class Manager : MonoBehaviour
{

    public GameObject CarPrefab;

    private bool isTraining = false;
    private int populationSize = 100;
    private int generationNumber = 0;
    private int[] layers = new int[] { 6, 128, 128, 128, 3 }; //6 input and 3 output
    private List<NeuralNetwork> nets;
    public List<CarController> CarList = null;
    private GameObject[] Spawn;
    private GameObject[] Targets;
    public float duration;
    private float max_duration = 180;

    public float bestfirstsector;
    public float bestround;
    public bool isTurn;


    private NeuralNetwork CurrentLeader;

    void Timer()
    {
        isTraining = false;
    }

    // Start is called before the first frame update
    void Start()
    {

        bestfirstsector = max_duration;
        bestround = max_duration;
        Spawn = GameObject.FindGameObjectsWithTag("Spawn");
        Targets = GameObject.FindGameObjectsWithTag("Target");


        foreach (GameObject Target in Targets)
        {
            Target.GetComponent<Target>().carArray = new CarController[populationSize];
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (isTurn)
        {
            if (isTraining == false)
            {
                if (generationNumber == 0)
                {
                    InitCarNeuralNetworks();
                }
                else
                {
                    nets.Sort();
                    CurrentLeader = new NeuralNetwork(nets[populationSize-1]);
                    for (int i = 0; i < populationSize / 2; i++)
                    {

                        nets[i] = new NeuralNetwork(nets[i + (populationSize / 2)]);
                        nets[i].Mutate();

                        nets[i + (populationSize / 2)] = new NeuralNetwork(nets[i + (populationSize / 2)]); 
                    }

                    for (int i = 0; i < populationSize; i++)
                    {
                        nets[i].SetFitness(0f);
                    }
                }

                generationNumber++;

                isTraining = true;
                Invoke("Timer", duration);
                CreateCarBodies();
            }
        }
       
    }


    void InitCarNeuralNetworks()
    {
        //population must be even, just setting it to 20 incase it's not
        if (populationSize % 2 != 0)
        {
            populationSize = 20;
        }

        nets = new List<NeuralNetwork>();


        for (int i = 0; i < populationSize; i++)
        {
            NeuralNetwork net = new NeuralNetwork(layers);
            net.Mutate();
            nets.Add(net);
        }
    }


    private void CreateCarBodies()
    {

        RemoveCar();

        foreach (GameObject Target in Targets)
        {
            Target.GetComponent<Target>().carArray = new CarController[populationSize];
        }




        CarController car;
        GameObject car_object;
        int spawn_duplication = populationSize / Spawn.Length;


        for (int i = 0; i < populationSize; i++)
        {
            car_object = ((GameObject)Instantiate(CarPrefab, Spawn[0].transform.position, Spawn[0].transform.rotation));
            car = car_object.GetComponent<CarController>();
            car.isPlayerControlled = false;
            car.manager = gameObject.GetComponent<Manager>();
            car.Init(nets[i]);
            CarList.Add(car); }




    }

    public void RemoveCar()
    {
        if (CarList != null)
        {
            for (int i = 0; i < CarList.Count; i++)
            {
                GameObject.Destroy(CarList[i].gameObject);
            }

            CarList = new List<CarController>();
        }
    }

    public void instantiateLeader()
    {
        if (CurrentLeader != null)
        {
            CarController car;
            GameObject car_object;
            car_object = ((GameObject)Instantiate(CarPrefab, Spawn[0].transform.position, Spawn[0].transform.rotation));
            car = car_object.GetComponent<CarController>();
            car.isPlayerControlled = false;
            car.manager = gameObject.GetComponent<Manager>();
            car.Init(CurrentLeader);
            CarList.Add(car);


        }
    }

}
