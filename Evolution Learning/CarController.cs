using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private bool initilized = false;
    private NeuralNetwork net;
    private Rigidbody rb;


    private float speed = 0;
    public float fit;
    public float speedkmh = 0;
    public float angle;
    public float torque;
    public bool handBrake;
    public bool isPlayerControlled = true;
    public bool ontrack = true;

    public float time1;
    public float time2;
    public float time3;

    public float sector1;
    public float round;


    public Manager manager;

    // Start is called before the first frame update
    void Start()
    {
        time1 = 0;
        time2 = 0;
        time3 = 0;
        rb = gameObject.GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (initilized == true)
        {
            
            
            speed = rb.velocity.magnitude;
            speedkmh = speed * 3.6f;
            
            

            

            
            

            float[] inputs = new float[6];
            inputs[0] = speed;
            inputs[1] = gameObject.GetComponent<LRay>().front;
            inputs[2] = gameObject.GetComponent<LRay>().left;
            inputs[3] = gameObject.GetComponent<LRay>().right;
            inputs[4] = gameObject.GetComponent<LRay>().left45;
            inputs[5] = gameObject.GetComponent<LRay>().right45;


            float[] output = net.FeedForward(inputs);
            angle = output[0];
            torque = output[1];
            handBrake = output[2] > 0;



          




            if (ontrack) {
                fit +=  speed*0.01f;
            }
            else { fit -= 10f* Time.deltaTime; }
           
           

         

            
              
            if(transform.position.y < -100)
            {
                fit = -100000;
            }
            net.AddFitness(fit);




            if(time2 != 0)
            {
                sector1 = time2 - time1;
                if (sector1 < manager.bestfirstsector)
                {
                    manager.bestfirstsector = sector1;
                    fit += 2000;
                }

            }
            if (time3 != 0)
            {
                if (sector1 != 0)
                {
                    round = time3 - time1;
                    if (round < manager.bestround && round >= 0 && round > sector1)
                    {
                        manager.bestround = round;
                        fit += 2000;
                    }
                }
            }



        }
    }


    /// <summary>
    /// Initializes car with a neural network.
    /// </summary>
    /// <param name="net">The neural network to initialize with</param>
    public void Init(NeuralNetwork net)
    {
        this.net = net;
        initilized = true;
    }


}
