using System;
using System.Collections;
using System.Collections.Generic;

public class BackPropagationNN
{
    int[] layer;
    Layer[] layers;
    public float learningRate = 0.0000333f;

    public BackPropagationNN(int[] layer)
    {
        this.layer = new int[layer.Length];
        for (int i = 0; i < layer.Length; i++)
        {
            this.layer[i] = layer[i];
        }

        layers = new Layer[layer.Length-1];

        for (int i = 0; i < layers.Length; i++)
        {
            layers[i] = new Layer(layer[i], layer[i+1],learningRate) ;
        }

    }

    public BackPropagationNN(BackPropagationNN copynetwork)
    {
        this.layer = new int[copynetwork.layer.Length];
        for (int i = 0; i < copynetwork.layer.Length; i++)
        {
            this.layer[i] = copynetwork.layer[i];
        }

        layers = new Layer[layer.Length - 1];

        for (int i = 0; i < layers.Length; i++)
        {
            layers[i] = new Layer(copynetwork.layers[i]);
        }

    }
    


    public float[] FeedForward(float[] inputs)
    {
        layers[0].FeedForward(inputs);
        for (int i = 1; i < layers.Length; i++)
        {
            layers[i].FeedForward(layers[i-1].outputs);
        }

        return layers[layers.Length - 1].outputs;

    }


    /// <summary>
    /// High level back porpagation
    /// </summary>
    public void BackProp(float[] expected)
    {
        // run over all layers backwards
        for (int i = layers.Length - 1; i >= 0; i--)
        {
            if (i == layers.Length - 1)
            {
                layers[i].BackPropOutput(expected); //back prop output
            }
            else
            {
                layers[i].BackPropHidden(layers[i + 1].gamma, layers[i + 1].weights); //back prop hidden
            }
        }

        //Update weights
        for (int i = 0; i < layers.Length; i++)
        {
            layers[i].UpdateWeights();
        }
    }



    



    public class Layer {

        int numberOfInputs; //number of neurons in previous layer
        int numberOfOutputs; //number of neurons in current layer



        public float learningRate;
        public float[] outputs;
        public float[] inputs;
        public float[,] weights;
        public float[,] weightsDelta;
        public float[] gamma;
        public float[] error;

        public Layer(int numberOfInputs, int numberOfOutputs, float learningRate)
        {
            this.numberOfInputs = numberOfInputs;
            this.numberOfOutputs = numberOfOutputs;
            this.learningRate = learningRate;

            outputs = new float[numberOfOutputs];
            inputs = new float[numberOfInputs];
            weights = new float[numberOfOutputs, numberOfInputs];
            weightsDelta = new float[numberOfOutputs, numberOfInputs];
            gamma = new float[numberOfOutputs];
            error = new float[numberOfOutputs];

            InitilizeWeights();
        }


        public Layer(Layer copyLayer)
        {
            this.numberOfInputs = copyLayer.numberOfInputs;
            this.numberOfOutputs = copyLayer.numberOfOutputs;
            this.learningRate = copyLayer.learningRate;

            outputs = deepCopy2d(copyLayer.outputs);
            inputs = deepCopy2d(copyLayer.inputs);
            weights = deepCopy3d(copyLayer.weights);
            weightsDelta = deepCopy3d(copyLayer.weightsDelta);
            gamma = deepCopy2d(copyLayer.gamma);
            error = deepCopy2d(copyLayer.error);

           // InitilizeWeights();
        }

        public void InitilizeWeights()
        {
            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weights[i, j] = UnityEngine.Random.Range(-0.5f, 0.5f);
                }
            }
        }


        public float[] FeedForward(float[] inputs)
        {
            this.inputs = inputs;
            for (int i = 0; i < numberOfOutputs; i++)
            {
                outputs[i] = 0;
                for (int j = 0; j < numberOfInputs; j++)
                {
                    outputs[i] += inputs[j] * weights[i, j];

                }

                outputs[i] = (float)Math.Tanh(outputs[i]);
            }
            return outputs;
        }


        public void BackPropHidden(float[] gammaForward, float[,]weightsForward)
        {
            for (int i = 0; i < numberOfOutputs; i++)
            {
                gamma[i] = 0;

                for(int j = 0; j < gammaForward.Length; j++)
                {
                    gamma[i] += gammaForward[j] * weightsForward[j, i];
                }

                gamma[i] *= TanHAbl(outputs[i]);

            }

            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weightsDelta[i, j] = gamma[i] * inputs[j];
                }
            }



        }



        public void BackPropOutput(float[] expected)
        {
            for (int i = 0; i < numberOfOutputs; i++)
                error[i] = outputs[i] - expected[i];

            for (int i = 0; i < numberOfOutputs; i++)
            {
                gamma[i] = error[i] * TanHAbl(outputs[i]);
            }
            for (int i = 0; i < numberOfOutputs; i++)
            {
                for (int j = 0; j < numberOfInputs; j++)
                {
                    weightsDelta[i, j] = gamma[i] * inputs[j];
                }
            }



        }

        public float TanHAbl(float value)
        {
            return 1 - (value * value);
        }

        

        public void UpdateWeights()
        {
            for(int i = 0; i < numberOfOutputs; i++)
            {
                for(int j = 0; j < numberOfInputs; j++)
                {
                    weights[i, j] -= weightsDelta[i, j] * learningRate;
                }
            }



        }


       public float[] deepCopy2d(float[] copy) {
            float [] retArray = new float[copy.Length];
            for (int i = 0; i < copy.Length; i++)
            {
                retArray[i] = copy[i];
            }
            return retArray;
        }
        public float[,] deepCopy3d(float[,] copy)
        {
            float[,] retArray = (float[,])copy.Clone();
            return retArray;
        }
    }
}
 
