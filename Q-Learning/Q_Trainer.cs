using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Q_Trainer : MonoBehaviour
{   
    public bool isTurn;
    public float duration;
    public long stateNr;
    public float epsilon;
   


    private GameObject[] Spawn;
    private GameObject[] Targets;

    Q_CarController car;
    public GameObject CarPrefab;

    public List<Q_CarController.QState> QSpecialTable = new List<Q_CarController.QState>();

    private bool isTraining = false;

    

    public BackPropagationNN netAcc;
    public BackPropagationNN netLeft;
    public BackPropagationNN netRight;
    public BackPropagationNN netDec;
    public BackPropagationNN netAccLeft;
    public BackPropagationNN netAccRight;
    public BackPropagationNN netDecLeft;
    public BackPropagationNN netDecRight;
    // Start is called before the first frame update
    void Start()
    {
        epsilon = 0.9f;
        Spawn = GameObject.FindGameObjectsWithTag("Spawn");
        Targets = GameObject.FindGameObjectsWithTag("Target");

        stateNr = 0;


        netAcc = new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });
        netLeft = new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });
        netRight = new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });
        netDec = new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });
        netAccLeft= new BackPropagationNN(new int[] {6, 25 , 100, 100, 25, 1 });
        netAccRight= new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });
        netDecLeft= new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });
        netDecRight= new BackPropagationNN(new int[] { 6, 25 , 100, 100, 25, 1 });

 

        foreach (GameObject Target in Targets)
        {
            Target.GetComponent<Target>().QcarArray = new Q_CarController[1];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isTurn)
        {
            if (isTraining == false)
            {
                isTraining = true;
                Invoke("Timer", duration);
                createCar();

            }
        }
    }




    void Timer()
    {
        isTraining = false;
    }

    public void createCar() {
        RemoveCar();

        GameObject car_object;
        car_object = ((GameObject)Instantiate(CarPrefab, Spawn[0].transform.position, Spawn[0].transform.rotation));

      
        car = car_object.GetComponent<Q_CarController>();
        foreach (GameObject Target in Targets)
        {
            Target.GetComponent<Target>().QcarArray = new Q_CarController[1];
        }
        car.QTrainer = gameObject.GetComponent<Q_Trainer>();
        car.epsilon = epsilon;
        car.stateNr = stateNr;
        
        car.AgentAcc = new BackPropagationNN(netAcc);
        car.AgentLeft = new BackPropagationNN(netLeft);
        car.AgentRight = new BackPropagationNN(netRight);
        car.AgentDec = new BackPropagationNN(netDec);
        car.AgentAccLeft = new BackPropagationNN(netAccLeft);
        car.AgentAccRight = new BackPropagationNN(netAccRight);
        car.AgentDecLeft = new BackPropagationNN(netDecLeft);
        car.AgentDecRight = new BackPropagationNN(netDecRight);

        car.PredictorAcc = new BackPropagationNN(netAcc);
        car.PredictorLeft = new BackPropagationNN(netLeft);
        car.PredictorRight = new BackPropagationNN(netRight);
        car.PredictorDec = new BackPropagationNN(netDec);
        car.PredictorAccLeft = new BackPropagationNN(netAccLeft);
        car.PredictorAccRight = new BackPropagationNN(netAccRight);
        car.PredictorDecLeft = new BackPropagationNN(netDecLeft);
        car.PredictorDecRight = new BackPropagationNN(netDecRight);

        car.QSpecialTable = QSpecialTable;



    }
    public void RemoveCar() {
        if (car != null)
        {
            epsilon = car.epsilon;
            stateNr = car.stateNr;

            QSpecialTable = car.QSpecialTable;

            netAcc = new BackPropagationNN(car.AgentAcc);
            netLeft = new BackPropagationNN(car.AgentLeft);
            netRight = new BackPropagationNN(car.AgentRight);
            netDec = new BackPropagationNN(car.AgentDec);
            netAccLeft = new BackPropagationNN(car.AgentAccLeft);
            netAccRight = new BackPropagationNN(car.AgentAccRight);
            netDecLeft = new BackPropagationNN(car.AgentDecLeft);
            netDecRight = new BackPropagationNN(car.AgentDecRight);
            GameObject.Destroy(car.gameObject);
        }

    }

    public void InstantiateLeader()
    { createCar(); 
    }
   




}



