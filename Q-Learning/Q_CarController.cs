using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Q_CarController : MonoBehaviour
{

    private Rigidbody rb;

    public float epsilon;
    public long batchstateNr;
    public long stateNr;
    public float angle;
    public float torque;

    float lerningRate = 1f; //Alpha
    float gamma = 0.9f;

    private float speed = 0;
    public int toadd;

    public Q_Trainer QTrainer;
    WheelDrive wc;

    public List<QState> QTable = new List<QState>();
    public List<QState> QSpecialTable;
    private QState oldState;

    public bool ontrack;

    public BackPropagationNN AgentAcc;
    public BackPropagationNN AgentLeft;
    public BackPropagationNN AgentRight;
    public BackPropagationNN AgentDec;
    public BackPropagationNN AgentAccLeft;
    public BackPropagationNN AgentAccRight;
    public BackPropagationNN AgentDecLeft;
    public BackPropagationNN AgentDecRight;

    public BackPropagationNN PredictorAcc;
    public BackPropagationNN PredictorLeft;
    public BackPropagationNN PredictorRight;
    public BackPropagationNN PredictorDec;
    public BackPropagationNN PredictorAccLeft;
    public BackPropagationNN PredictorAccRight;
    public BackPropagationNN PredictorDecLeft;
    public BackPropagationNN PredictorDecRight;


    public Action action;
    public float qacc;
    public float qleft;
    public float qright;
    public float qdec;
    public float qleftacc;
    public float qrightacc;
    public float qrightdec;
    public float qleftdec;

    private float lastspeed;
    public float stateq;

    public int qspecialLength;

    public Action stateaction;
    // Start is called before the first frame update
    void Start()
    {
        wc = gameObject.GetComponent<WheelDrive>(); //Lol WC
        wc.iseve = false;
        rb = gameObject.GetComponent<Rigidbody>();
        batchstateNr = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
       wc.iseve = false;
      
        

        processState();

    }
     

    public struct QState
    {
       public float speed;
       public float front;
       public float left;
       public float right;
       public float left45;
       public float right45;
       public Action action;
       public float QValue;


        public QState(float speed,float front,float left,float right,float left45,float right45, Action action, float QValue)
        {
            this.speed = speed;
            this.front = front;
            this.left = left;
            this.right = right;
            this.left45 = left45;
            this.right45 = right45;
            this.action = action;
            this.QValue = QValue;


        }
    }



    public enum Action
    {
        Accelerate,
        Left,
        Right,
        Decelerate,
        LeftAcc,
        RightAcc,
        LeftDec,
        RightDec,
    }

    public Action chooseAction(QState state)
    {
        float rand = UnityEngine.Random.value;
        if (rand < epsilon)
        {
            int action = UnityEngine.Random.Range(0, 8);
            return (Action)action;
        }

        else
        {
            float[] actionValues = new float[8];
            actionValues[0] = AgentAcc.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];
            actionValues[1] = AgentLeft.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45})[0];
            actionValues[2] = AgentRight.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];
            actionValues[3] = AgentDec.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];
            actionValues[4] = AgentAccLeft.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];
            actionValues[5] = AgentAccRight.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];
            actionValues[6] = AgentDecLeft.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];
            actionValues[7] = AgentDecRight.FeedForward(new float[] { state.speed, state.front, state.left, state.right, state.left45, state.right45 })[0];


            qacc = actionValues[0];
            qleft = actionValues[1];
            qright = actionValues[2];
            qdec = actionValues[3];
            qleftacc = actionValues[4];
            qrightacc = actionValues[5];
            qleftdec = actionValues[6];
            qrightdec = actionValues[7];


            float max = actionValues[0];
            int index = 0;
            for (int i = 0; i < actionValues.Length; i++)
            {
                if (actionValues[i] > max)
                {
                    max = actionValues[i];
                    index = i;
                }

            }
            action = (Action)index;
            return (Action)index;
        }
    }



    public void processState()
    {

        //calculate reward
        speed = rb.velocity.magnitude;
        float reward = 1;
        reward += toadd;
        

        if (!ontrack && batchstateNr >= 100)
       { reward -= 1000; }

        float oldQ;
        if(batchstateNr == 0) {  oldQ = 0; } else { oldQ = QTable[(int)batchstateNr - 1].QValue; }
        
        QState newState = new QState(speed,
        gameObject.GetComponent<LRay>().front,
        gameObject.GetComponent<LRay>().left,
        gameObject.GetComponent<LRay>().right,
        gameObject.GetComponent<LRay>().left45,
        gameObject.GetComponent<LRay>().right45,
        0,
        0);

        Action choosenAct = chooseAction(newState);
        newState.action = choosenAct;
    


        switch (choosenAct)
        {
            case Action.Accelerate: torque = 1; angle = 0; break;
            case Action.Left: torque = 0; angle = 1; break;
            case (Action)2: torque = 0; angle = -1; break;
            case (Action)3: torque = -1; angle = 0; break;
            case (Action)4: torque = 1; angle = 1; break;
            case (Action)5: torque = 1; angle = -1; break;
            case (Action)6: torque = -1; angle = 1; break;
            case (Action)7: torque = -1; angle = -1; break;
        }

        float QValue = calculateQValue(newState,reward, oldQ);
        QValue = (float)Math.Tanh(QValue);
       
        
        QState currentState = oldState;
        currentState.QValue = QValue;

        //Debug.Log(QValue);

        QTable.Add(currentState);
        
        int randomReplay = UnityEngine.Random.Range(0, QTable.Count);
        int k = 1;
        if(QTable.Count >= 50000) { k = 2; Debug.Log("50000"); }
        if(QTable.Count >= 100000) { k = 3; Debug.Log("100000"); }
        if (QTable.Count >= 150000) { k = 4; Debug.Log("150000"); }
        for (int i = 0; i < k; i++) { 
            switch (QTable[randomReplay].action)
            {
            case Action.Accelerate:
            AgentAcc.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
            AgentAcc.BackProp(new float[] { QTable[randomReplay].QValue }); break;
            case Action.Left:
                AgentLeft.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentLeft.BackProp(new float[] { QTable[randomReplay].QValue }); ; break; 
            case (Action)2:
                AgentRight.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentRight.BackProp(new float[] { QTable[randomReplay].QValue }); ; break; 
            case (Action)3:
                AgentDec.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentDec.BackProp(new float[] { QTable[randomReplay].QValue }); ; break;
            case (Action)4:
                AgentAccLeft.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentAccLeft.BackProp(new float[] { QTable[randomReplay].QValue }); ; break; 
            case (Action)5:
                AgentAccRight.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentAccRight.BackProp(new float[] { QTable[randomReplay].QValue }); ; break;
            case (Action)6:
                AgentDecLeft.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentDecLeft.BackProp(new float[] { QTable[randomReplay].QValue }); ; break; 
            case (Action)7:
                AgentDecRight.FeedForward(new float[] { QTable[randomReplay].speed, QTable[randomReplay].front, QTable[randomReplay].left, QTable[randomReplay].right, QTable[randomReplay].left45, QTable[randomReplay].right45 });
                AgentDecRight.BackProp(new float[] { QTable[randomReplay].QValue }); ; break; 
            }
        }
        if (randomReplay >= QTable.Count/1.5f && QSpecialTable.Count != 0) {
            randomReplay = UnityEngine.Random.Range(0, QSpecialTable.Count);
            switch (QSpecialTable[randomReplay].action)
            {
                case Action.Accelerate:
                    AgentAcc.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentAcc.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); break;
                case Action.Left:
                    AgentLeft.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentLeft.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
                case (Action)2:
                    AgentRight.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentRight.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
                case (Action)3:
                    AgentDec.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentDec.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
                case (Action)4:
                    AgentAccLeft.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentAccLeft.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
                case (Action)5:
                    AgentAccRight.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentAccRight.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
                case (Action)6:
                    AgentDecLeft.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentDecLeft.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
                case (Action)7:
                    AgentDecRight.FeedForward(new float[] { QSpecialTable[randomReplay].speed, QSpecialTable[randomReplay].front, QSpecialTable[randomReplay].left, QSpecialTable[randomReplay].right, QSpecialTable[randomReplay].left45, QSpecialTable[randomReplay].right45 });
                    AgentDecRight.BackProp(new float[] { QSpecialTable[randomReplay].QValue }); ; break;
            } }

        stateaction = currentState.action;
        stateq = currentState.QValue;
        oldState = newState;

        

        if (stateNr > 20000)
        {
            if (epsilon > 0.05f)
            {
                epsilon -= 4e-7f;
            }
            else if (epsilon <= 0.5) { epsilon = 0.05f; }

            
        }

        if(toadd != 0)
        {
            QSpecialTable.Add(currentState);

        }
        
        batchstateNr += 1;
        stateNr += 1;
        toadd = 0;


        qspecialLength = QSpecialTable.Count;
        if (!ontrack && batchstateNr >= 100)
        {
            QSpecialTable.Add(currentState);
            QTrainer.createCar();
        }
    }




    private float calculateQValue(QState now,float reward,float oldQ)
    {
        float currentQ;
        float maxfuture = (float)ATanh((double)predictMaxFuture(now));
        

        currentQ = oldQ + lerningRate * (reward + gamma * maxfuture - oldQ);

        return currentQ;




    }

    public float predictMaxFuture(QState now) {
        float[] actionValues = new float[8];
        actionValues[0] = PredictorAcc.FeedForward(new float[] { now.speed,now.front,now.left,now.right,now.left45,now.right45})[0];
        actionValues[1] = PredictorLeft.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45})[0];
        actionValues[2] = PredictorRight.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45 })[0];
        actionValues[3] = PredictorDec.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45 })[0];
        actionValues[4] = PredictorAccLeft.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45})[0];
        actionValues[5] = PredictorAccRight.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45 })[0];
        actionValues[6] = PredictorDecLeft.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45})[0];
        actionValues[7] = PredictorDecRight.FeedForward(new float[] { now.speed, now.front, now.left, now.right, now.left45, now.right45 })[0];

        float max = actionValues[0];
        for(int i = 0; i < actionValues.Length; i++)
        {
            if(actionValues[i] > max)
            {
                max = actionValues[i];
            }

        }

        return max;
        
    }



    public static double ATanh(double x)
    {
        return (Math.Log(1 + x) - Math.Log(1 - x)) / 2;
    }

}
